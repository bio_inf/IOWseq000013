# Analysis of archaeal V4 16S amplicons
# IOWseq000013

# load packages
require(tidyverse)
require(furrr)
require(dada2)
require(ShortRead)
require(ggplot2)
require(gridExtra)
require(Biostrings)
require(scales)

# R 4.1.0
packageVersion("dada2")
# '1.20.0'

# working directory
setwd("/bio/Analysis_data/IOWseq000013/Intermediate_results/dada2_r1")
# save.image("archaea.Rdata")

# defining functions

# Wrapper function to create multi-page pdf with base quality plots
# Argument 'file_base' will be extended by '_R1.pdf' and '_R2.pdf'
quality_check <- function(
  R1, 
  R2, 
  file_base = "QualityProfile",
  cpus = 1
) {
  
  # start parallel session
  plan(multicore, workers = cpus)
  
  # R1 profiles
  QualityProfileFs <- future_map(
    1:length(R1),
    function(x) {
      tmp <- list()
      tmp[[1]] <- plotQualityProfile(R1[x])
      return(tmp)
    }
  )
  pdf(paste0(file_base, "_R1.pdf"))
  for(i in 1:length(R1)) {
    do.call("grid.arrange", QualityProfileFs[[i]])  
  }
  dev.off()
  
  # R2 profiles
  QualityProfileRs <- future_map(
    1:length(R2),
    function(x) {
      tmp <- list()
      tmp[[1]] <- plotQualityProfile(R2[x])
      return(tmp)
    }
  )
  pdf(paste0(file_base, "_R2.pdf"))
  for(i in 1:length(R2)) {
    do.call("grid.arrange", QualityProfileRs[[i]])  
  }
  dev.off()
}

# Wrapper function for filtering parameter screening
screen_settings <- function(
  sample.names, 
  fnFs, 
  fnRs, 
  range_maxEE, 
  range_truncLen, 
  cpus = 1
) {
  
  # construct output
  # proportion of total sequences retained, and mean
  # quantiles from min to max in 10% increments
  out <- matrix(NA, ncol = 17, nrow = nrow(range_maxEE) * nrow(range_truncLen))
  colnames(out) <- c(
    "truncLen_R1",
    "truncLen_R2",
    "maxEE_R1",
    "maxEE_R2",
    "prop_total",
    "prop_mean",
    "min",
    paste0("q", seq(10, 90, 10)),
    "max"
  )
  
  # fill first 4 columns
  out[, 1] <- rep(range_truncLen[, 1], each = nrow(range_maxEE))
  out[, 2] <- rep(range_truncLen[, 2], each = nrow(range_maxEE))
  out[, 3] <- rep(range_maxEE[, 1], times = nrow(range_truncLen))
  out[, 4] <- rep(range_maxEE[, 2], times = nrow(range_truncLen))
  
  # create tmp folder for output
  tmp_filtFs <- file.path("tmp", paste0(sample.names, "_F_filt.fastq"))
  tmp_filtRs <- file.path("tmp", paste0(sample.names, "_R_filt.fastq"))
  names(tmp_filtFs) <- sample.names
  names(tmp_filtRs) <- sample.names
  
  # loop through each combination of truncLen and maxEE
  for(i in 1:nrow(out)) {
    print(paste0("truncLen ", out[i, 1], ",", out[i, 2], " with maxEE ", out[i, 3], ",", out[i, 4]))
    tmp <- filterAndTrim(
      fwd = fnFs, 
      filt = tmp_filtFs, 
      rev = fnRs, 
      filt.rev = tmp_filtRs,
      truncLen = c(out[i, 1], out[i, 2]),
      maxN = 0,
      minQ = 2,
      maxEE = c(out[i, 3], out[i, 4]), 
      truncQ = 0, 
      rm.phix = TRUE,
      compress = F,
      multithread = cpus
    )
    
    # fill output table
    out[i, "prop_total"] <- round(sum(tmp[, 2])/sum(tmp[, 1]), 4)
    out[i, "prop_mean"] <- round(mean(tmp[, 2]/tmp[, 1]), 4)
    out[i, 7:17] <- round(quantile(tmp[, 2]/tmp[, 1], seq(0, 1, 0.1)), 4)
  }
  
  # remove tmp folder and return output table
  system("rm -rf tmp/")
  return(out)
}

# Further curation of taxonomic paths: removing NAs and uncultured
# append unclassified to last classified level instead of NA
curate_taxpath <- function(tax) {
  out <- tax
  out[out == "uncultured"] <- NA
  k <- ncol(out) - 1
  for (i in 2:k) {
    if (sum(is.na(out[, i])) > 1) {
      test <- out[is.na(out[, i]), ]
      for (j in 1:nrow(test)) {
        if (sum(is.na(test[j, i:(k + 1)])) == length(test[j, i:(k + 1)])) {
          test[j, i] <- paste(test[j, (i - 1)], "_unclassified", sep = "")
          test[j, (i + 1):(k + 1)] <- test[j, i]
        }
      }
      out[is.na(out[, i]), ] <- test
    }
    if (sum(is.na(out[, i])) == 1) {
      test <- out[is.na(out[, i]), ]
      if (sum(is.na(test[i:(k + 1)])) == length(test[i:(k + 1)])) {
        test[i] <- paste(test[(i - 1)], "_unclassified", sep = "")
        test[(i + 1):(k + 1)] <- test[i]
      }
      out[is.na(out[, i]),] <- test
    }
  }
  out[is.na(out[, (k + 1)]), (k + 1)] <- paste(out[is.na(out[, (k + 1)]), k], "_unclassified", sep = "")
  return(out)
}

# get sample names from files
sample.names <- gsub("_.*", "", list.files("/bio/Analysis_data/IOWseq000013/Validated_data", pattern = "R1.fastq.gz"))

# Specify path to input sequences
path <- "/bio/Analysis_data/IOWseq000013/Validated_data"
fnFs <- paste0(path, "/", sample.names, "_R1.fastq.gz")
fnRs <- paste0(path, "/", sample.names, "_R2.fastq.gz")
names(fnFs) <- sample.names
names(fnRs) <- sample.names

# Quality check
quality_check(
  fnFs,
  fnRs,
  file_base = "QualityProfile",
  cpus = 12
)

# Place filtered files in LGC_filtered/ subdirectory
filtFs <- file.path("Filtered", paste0(sample.names, "_F_filt.fastq"))
filtRs <- file.path("Filtered", paste0(sample.names, "_R_filt.fastq"))
names(filtFs) <- sample.names
names(filtRs) <- sample.names

# Considerations for trimming:
# 2x300bp (minus 17bp fwd and 20bp rev primer)
# expected max length: 300bp
# min overlap: 30bp
# It is recommended to trim to just enough for the required length for sufficient overlap
# Caution: don't remove too much

# Define ranges for truncLen
range_truncLen <- matrix(
  c(130, 210,
    140, 200,
    150, 180,
    160, 170),
  nrow = 4,
  ncol = 2,
  byrow = T
)

# Define ranges for maxEE
range_maxEE <- matrix(
  c(1, 1,
    2, 2,
    3, 3),
  nrow = 3,
  ncol = 2,
  byrow = T
)

# run screening
screen_filt_settings <- screen_settings(
  sample.names,
  fnFs,
  fnRs,
  range_maxEE,
  range_truncLen,
  cpus = 12
)

# show output
pdf("Parameter_screening.pdf", width = 7, height = 7)
plot(
  screen_filt_settings[, "prop_total"],
  screen_filt_settings[, "q90"] - screen_filt_settings[, "q10"],
  col = rep(rainbow(nrow(range_maxEE)), nrow(range_truncLen)),
  pch = 16
)
text(
  screen_filt_settings[, "prop_total"],
  screen_filt_settings[, "q90"] - screen_filt_settings[, "q10"],
  pos = 2,
  col = adjustcolor("black", alpha.f = 0.5)
)
dev.off()

# Run trimming with optimal parameters
filt.out <- filterAndTrim(
  fwd = fnFs, 
  filt = filtFs, 
  rev = fnRs, 
  filt.rev = filtRs,
  truncLen = c(140, 200),
  maxN = 0,
  minQ = 2,
  maxEE = c(1, 1), 
  truncQ = 0, 
  rm.phix = TRUE,
  compress = F,
  multithread = 12
)

# Repeat quality check after trimming
quality_check(
  filtFs,
  filtRs,
  file_base = "QualityProfileFiltered"
)

# Learn error rates
errF <- learnErrors(filtFs, multithread = 64, randomize = TRUE, verbose = 1, MAX_CONSIST = 10)
errR <- learnErrors(filtRs, multithread = 64, randomize = TRUE, verbose = 1, MAX_CONSIST = 10)

# try modified error function
loessErrfun2 <- function (trans)
{
  qq <- as.numeric(colnames(trans))
  est <- matrix(0, nrow = 0, ncol = length(qq))
  for (nti in c("A", "C", "G", "T")) {
    for (ntj in c("A", "C", "G", "T")) {
      if (nti != ntj) {
        errs <- trans[paste0(nti, "2", ntj), ]
        tot <- colSums(trans[paste0(nti, "2", c("A",
                                                "C", "G", "T")), ])
        rlogp <- log10((errs + 1)/tot)
        rlogp[is.infinite(rlogp)] <- NA
        df <- data.frame(q = qq, errs = errs, tot = tot,
                         rlogp = rlogp)
        mod.lo <- loess(rlogp ~ q, df, weights = log10(tot), span = 2)
        pred <- predict(mod.lo, qq)
        maxrli <- max(which(!is.na(pred)))
        minrli <- min(which(!is.na(pred)))
        pred[seq_along(pred) > maxrli] <- pred[[maxrli]]
        pred[seq_along(pred) < minrli] <- pred[[minrli]]
        est <- rbind(est, 10^pred)
      }
    }
  }
  MAX_ERROR_RATE <- 0.25
  MIN_ERROR_RATE <- 1e-07
  est[est > MAX_ERROR_RATE] <- MAX_ERROR_RATE
  est[est < MIN_ERROR_RATE] <- MIN_ERROR_RATE
  err <- rbind(1 - colSums(est[1:3, ]), est[1:3, ], est[4,
  ], 1 - colSums(est[4:6, ]), est[5:6, ], est[7:8, ], 1 -
    colSums(est[7:9, ]), est[9, ], est[10:12, ], 1 - colSums(est[10:12,
    ]))
  rownames(err) <- paste0(rep(c("A", "C", "G", "T"), each = 4),
                          "2", c("A", "C", "G", "T"))
  colnames(err) <- colnames(trans)
  return(err)
}

errF <- learnErrors(filtFs, errorEstimationFunction = loessErrfun2, multithread = 64, randomize = TRUE, verbose = 1, MAX_CONSIST = 10)
errR <- learnErrors(filtRs, errorEstimationFunction = loessErrfun2, multithread = 64, randomize = TRUE, verbose = 1, MAX_CONSIST = 10)

# check convergence of error estimation and plot error profiles
pdf("ErrorProfiles.pdf")
barplot(log10(dada2:::checkConvergence(errF) + 1), main = "Convergence_fwd")
barplot(log10(dada2:::checkConvergence(errR) + 1), main = "Convergence_rev")
plotErrors(errF, nominalQ = TRUE)
plotErrors(errR, nominalQ = TRUE)
dev.off()

# Dereplicate and denoise samples
dadaFs <- dada(filtFs, err = errF, multithread = 64, pool = TRUE)
dadaRs <- dada(filtRs, err = errR, multithread = 64, pool = TRUE)

# Merge reads
mergers <- mergePairs(
  dadaFs,
  filtFs,
  dadaRs,
  filtRs,
  minOverlap = 10,
  verbose = TRUE
)

# Create sequence table
seqtab <- makeSequenceTable(mergers)
dim(seqtab)
# 7119 ASVs

# Remove chimeras
seqtab.nochim <- removeBimeraDenovo(seqtab, method = "consensus", multithread = 60, verbose = TRUE, minFoldParentOverAbundance = 2)
ncol(seqtab.nochim)/ncol(seqtab)
# 40% of ASVs retained
summary(rowSums(seqtab.nochim)/rowSums(seqtab))
#   Min. 1st Qu.  Median    Mean 3rd Qu.    Max.
# 0.7750  0.8305  0.8432  0.8431  0.8681  0.9040

# Inspect ASV length distribution (in the end this was done in the separate batches with reorient workspace)
table(nchar(colnames(seqtab.nochim)))
table(rep(nchar(colnames(seqtab.nochim)), colSums(seqtab.nochim)))
uniquesToFasta(
  seqtab.nochim[, nchar(colnames(seqtab.nochim)) < 250 | nchar(colnames(seqtab.nochim)) > 252], 
  "check.fasta"
)

# Remove potential junk sequences and singletons
seqtab.nochim2 <- seqtab.nochim[, nchar(colnames(seqtab.nochim)) %in% 250:252 & colSums(seqtab.nochim) > 1]
dim(seqtab.nochim2)
# 2736 ASVs
ncol(seqtab.nochim2)/ncol(seqtab)
# 38% of ASVs retained
summary(rowSums(seqtab.nochim2)/rowSums(seqtab))
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max.
# 0.7697  0.8213  0.8387  0.8390  0.8657  0.9015

# number of sequence
getN <- function(x) sum(getUniques(x))
track <- cbind(
  filt.out,
  sapply(dadaFs, getN), 
  sapply(dadaRs, getN), 
  sapply(mergers, getN), 
  rowSums(seqtab.nochim), 
  rowSums(seqtab.nochim2)
)
colnames(track) <- c("Clipped", "Filtered", "Denoised_fwd", "Denoised_rev", "Merged", "Nochim", "Tabled")
rownames(track) <- c(sample.names)
track <- data.frame(track)
track.perc <- apply(track, 2, function(x) x/track$Clipped)

# taxonomic classification with silva
tax <- assignTaxonomy(
  seqtab.nochim2, 
  "/bio/Databases/SILVA/v138.1/dada2/silva_nr99_v138.1_train_set.fa.gz",
  multithread = 64,
  minBoot = 0,
  outputBootstraps = T,
  taxLevels = c("Kingdom", "Phylum", "Class", "Order", "Family", "Genus")
)

# Remove unwanted lineages
# keep archaea and bacteria, but remove chloroplast and mitochondrial sequences
tax.good <- lapply(tax, function(x) {
  x[tax$tax[, 1] %in% c("Archaea", "Bacteria") & 
      !grepl("[Cc]hloroplast", tax$tax[, 4]) &
      !grepl("[Mm]itochondria", tax$tax[, 5])
    , ] 
})

# Determine most suitable bootstrap cut-off
apply(tax.good$boot, 2, summary)

# Applying bootstrap filter at pre-determined cut-off
tax.filt <- tax.good$tax
tax.filt[tax.good$boot < 70] <- NA
# remove all without kingdom assignment
tax.filt <- tax.filt[!is.na(tax.filt[, 1]), ]

# Which proportion of sequences is affected?
asv.filt <- t(seqtab.nochim2[, rownames(tax.filt)])
tmp <- data.frame(apply(tax.filt, 2, function(x) sum(rowSums(asv.filt[is.na(x), ]))/sum(colSums(asv.filt))))
colnames(tmp) <- "proportion"
tmp
rm(tmp)

# Further curation of taxonomic paths
# append unclassified to last classified level instead of NA
tax.filt.clean <- curate_taxpath(tax.filt)

colnames(tax.filt.clean) <- c("domain", "phylum", "class", "order", "family", "genus")

# fix sample names
meta <- read.table("/bio/Raw_data/IOWseq000013/sample_metadata.tsv", h = T, sep = "\t", check.names = F)
meta$filename <- gsub("_.*$", "", meta$forward_read_file_name)
rownames(track) <- meta$seq_id[match(rownames(track), meta$filename)]
rownames(seqtab.nochim2) <- meta$seq_id[match(rownames(seqtab.nochim2), meta$filename)]
track <- track[meta$seq_id, ]
seqtab.nochim2 <- seqtab.nochim2[meta$seq_id, ]

# write output files
write.table(track, "nseqs_all.txt", quote = F, sep = "\t")
write.table(t(seqtab.nochim2), "asvtab.txt", quote = F, sep = "\t")
uniquesToFasta(seqtab.nochim2, "seqs.fasta")
write.table(tax.filt.clean, "tax_dada2.txt", quote = F, sep = "\t")