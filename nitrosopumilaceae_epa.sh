# SINA alignment (take q2 env)
conda activate qiime2-2021.2
sina -i nitrosopumilus_asvs.fasta -r /bio/Databases/SILVA/v138.1/SILVA_138.1_SSURef_NR99_12_06_20_opt.arb -o nitrosopumilus_asvs_align.fasta -t all -p 60 --overhang=remove --insertion=forbid

# Take 16S from genomes (strain: e[G], n[G], w[G]) in SILVA Ref
# Nitrosopumilaceae, Nitrososphaeraceae, Nitrosotaleaceae
# 41 seqs
silva_genome_reference.fasta

# take GTDB (all, not just cluster reps) 16S from nitrosopumilus
# search for Candidatus Nitrosopumilus in metadata column: ssu_silva_taxonomy
cd /bio/Databases/GTDB/R207
cut -f1,106 ar53_metadata_r207.tsv | grep "Candidatus Nitrosopumilus" > /bio/Analysis_data/IOWseq000013/Intermediate_results/nitrosopumilus_epa/gtdb_reference_nitrosopumilus.txt
# extracting fasta sequence
cd /bio/Analysis_data/IOWseq000013/Intermediate_results/nitrosopumilus_epa
cut -f1 gtdb_reference_nitrosopumilus.txt | grep -A1 -F -f - /bio/Analysis_data/IOWseq000019_EVAR_MSM105_16S/Intermediate_results/Beggiatoaceae_phylogeny/ssu_all_r207.fna | sed '/^--$/d' > gtdb_reference_nitrosopumilus.fasta

# take any NCBI refseq/genbank nitrosopumilus not already represented in GTDB (status Dec 2022)
cd /bio/Analysis_data/IOWseq000013/Intermediate_results/nitrosopumilus_epa
cut -f1 gtdb_reference_nitrosopumilus.txt | cut -d'_' -f2,3 | cut -d'.' -f1 | grep -v -w -F -f - <(grep "Nitrosopumilus" /bio/Analysis_data/IOWseq000019_EVAR_MSM105_16S/Intermediate_results/Beggiatoaceae_phylogeny/assembly_summary_refseq_taxpath.txt) | grep ";Archaea;" > tmp
cut -f1 gtdb_reference_nitrosopumilus.txt | cut -d'_' -f2,3 | cut -d'.' -f1 | grep -v -w -F -f - <(grep "Nitrosopumilus" /bio/Analysis_data/IOWseq000019_EVAR_MSM105_16S/Intermediate_results/Beggiatoaceae_phylogeny/assembly_summary_genbank_taxpath.txt) | grep ";Archaea;" | grep -v -F -f <(cut -f1 tmp | cut -d'.' -f1 | cut -d'_' -f2) | cat tmp - > ncbi_reference_nitrosopumilus.txt
rm tmp
# check if download path for ssu exists (in R)
conda activate r-4.2.0
require(tidyverse)
require(httr)
dat <- read.table("ncbi_reference_nitrosopumilus.txt", h = F, sep = "\t", quote = "", comment.char = "")
dat$download_rna <- paste0(dat$V20, "/", basename(dat$V20), "_rna_from_genomic.fna.gz")
dat$rna_available <- !map_lgl(dat$download_rna, http_error)
write.table(dat[dat$rna_available, ], "ncbi_reference_nitrosopumilus_rna.txt", row.names = F, col.names = F, sep = "\t", quote = F)
# download rna gene files from NCBI
mkdir ncbi_download
cd ncbi_download
cut -f25 ../ncbi_reference_nitrosopumilus_rna.txt | parallel -j40 wget
ls -1 *.gz | parallel -j10 gunzip
conda activate bbmap-39.01
ls -1 *.fna | sed 's/\.fna//' | while read line
do
  reformat.sh in=${line}.fna out=${line}_lin.fna fastawrap=50000
  grep -A1 "16S ribosomal RNA" ${line}_lin.fna | sed '/^--$/d' | sed "s/^>/>${line}~/"
done > ../ncbi_reference_nitrosopumilus.fasta
cd ..

# take all Nitrosopumilus 16S from SILVA
# filter to Baltic based on lat/lon or country
# 2 seqs found: already included in silva_genome_reference.fasta

# reconstruct 16S from Baltic Sea metaG --> geobox for Baltic --> restrict by lat/lon --> phyloflash
# search query: tax_tree(410657) AND geo_box1(52.5546,9.0508,66.6015,30.0156) AND library_strategy="WGS"
# 318 suitable metaG found
cd Baltic_metaG/
Baltic_metaG_filtered.txt
# on olorin
cut -f40 Baltic_metaG_filtered.txt | sed '1d' | tr ';' '\n' | sed 's/^/http:\/\//' > download.links
aria2c -i download.links -c --dir download --max-tries=20 --retry-wait=5 --max-connection-per-server=1 --max-concurrent-downloads=100 &>> download.log
# quick QC check on R1 read only --> identify presence of adapters (all libraries should be paired, but 1 sample not archived correctly --> ignore)
module load fastqc
cd qc
fastqc -t 100 -o . ../download/*_1.fastq.gz
# unzip
grep "Adapter" ./*/summary.txt | grep -v "PASS"
# right clip necessary: shouldn't hurt to run on all samples
# at HLRN:
cd /scratch/usr/mvbchass/Analysis_data/Baltic
cut -f79 Baltic_metaG_filtered.txt | sed '1d' > snakemake_wf/metaG_accession_list.txt
sed '/SRR3745614/d' -i snakemake_wf/metaG_accession_list.txt
conda activate /scratch/usr/mvbchass/.conda/snakemake-env
module load java
cd /scratch/usr/mvbchass/Repos/workflow_templates/metaG_Illumina_PE
snakemake -s /scratch/usr/mvbchass/Analysis_data/Baltic/snakemake_wf/Snakefile --use-conda --conda-create-envs-only --cores 1 -np
snakemake -s /scratch/usr/mvbchass/Analysis_data/Baltic/snakemake_wf/Snakefile --use-conda -j 100 --cluster-config /scratch/usr/mvbchass/Analysis_data/Baltic/snakemake_wf/slurm.yaml --cluster "sbatch --export=ALL --no-requeue -t {cluster.time} -N {cluster.nodes} -p {cluster.partition} -c {threads} -q {cluster.qos} -A {cluster.account} -J {rulename}.{jobid}" -np
#SRR2053300
#SRR3727506
#SRR3727510
#SRR3727514 # no luck with assembly...
#SRR3745603
#SRR3745604
#SRR3745605
#SRR3745606
#SRR3745608
#SRR3745609
#SRR3745610
#SRR3745611
#SRR3745612
#SRR3745613
# error in phyloflash, but nothing mapped to nitrosopumilus
cat taxonomy/*/*_all_vsearch_final.csv | grep "Archaea;" # 116
cat taxonomy/*/*_all_vsearch_final.csv | grep "Candidatus Nitrosopumilus" > Baltic_metaG_nitrosopumilus.txt # 51
cat taxonomy/*/*.all.final.fasta > Baltic_metaG_all_ssu.fasta
conda activate /scratch/usr/mvbchass/Analysis_data/Mara/v1/snakemake_wf/.snakemake/conda/04f0f5b5e492c9fe269140b0c060559b
reformat.sh in=Baltic_metaG_all_ssu.fasta out=Baltic_metaG_all_ssu_lin.fasta fastawrap=500000
cut -f1 Baltic_metaG_nitrosopumilus.txt | grep -A1 -w -F -f - Baltic_metaG_all_ssu_lin.fasta | sed '/^--$/d' > Baltic_metaG_nitrosopumilus.fasta

# align all full length 16S refs
conda activate qiime2-2021.2
sina -i silva_genome_reference.fasta -r /bio/Databases/SILVA/v138.1/SILVA_138.1_SSURef_NR99_12_06_20_opt.arb -o silva_genome_reference_align.fasta -t all -p 60 --overhang=remove --insertion=forbid
sina -i gtdb_reference_nitrosopumilus.fasta -r /bio/Databases/SILVA/v138.1/SILVA_138.1_SSURef_NR99_12_06_20_opt.arb -o gtdb_reference_nitrosopumilus_align.fasta -t all -p 60 --overhang=remove --insertion=forbid
sina -i ncbi_reference_nitrosopumilus.fasta -r /bio/Databases/SILVA/v138.1/SILVA_138.1_SSURef_NR99_12_06_20_opt.arb -o ncbi_reference_nitrosopumilus_align.fasta -t all -p 60 --overhang=remove --insertion=forbid
sina -i Baltic_metaG_nitrosopumilus.fasta -r /bio/Databases/SILVA/v138.1/SILVA_138.1_SSURef_NR99_12_06_20_opt.arb -o Baltic_metaG_nitrosopumilus_align.fasta -t all -p 60 --overhang=remove --insertion=forbid

# concatenate all alignments for manual curation
cat *_align.fasta > all_align.fasta
# manually clean in seaview

# I did not check yet redundancy between silva and ncbi/gtdb
# remove additional redundancy based on gene accnos in silva reference
grep '^>' silva_genome_reference_align.fasta | sed 's/^>//' | cut -d'.' -f1 | grep -F -f - <(cat ncbi_reference_nitrosopumilus_align.fasta gtdb_reference_nitrosopumilus_align.fasta) | sed 's/^>//' | sed 's/ .*$//' > tmp_remove.accnos
# remove those manually when curating alignment
# manually filter out any remaining redundancy and include metadata on taxonomy and isolation source / sampled environment
# convert metadata to fasta
sed '1d' nitrosopumilus_metadata.txt | cut -f1,2 | sed 's/^/>/' | tr '\t' '\n' > all_ref_align_curated_uniq.fasta

# infer reference tree
conda activate modeltestng-0.1.7
modeltest-ng -d nt -i all_ref_align_curated_uniq.fasta -o reference_modeltest.out -p 80 -t ml
conda deactivate
# not run again for uniq seq set

# raxml
conda activate raxmlng-1.1.0
raxml-ng --check --msa all_ref_align_curated_uniq.fasta --model TPM3+I+G4 --prefix tmp_raxml
raxml-ng --all --msa all_ref_align_curated_uniq.fasta --prefix reference_uniq_raxml --threads 38 --workers 1 --model TPM3+I+G4 --tree pars{5},rand{5} --bs-trees 200 > reference_uniq_raxml.log 2>&1
# there might not be enough informative sites for a reliable phylogeny

# phylogenetic placement of ASVs
conda activate qiime2-2021.2
epa-ng --ref-msa all_ref_align_curated_uniq.fasta --tree reference_uniq_raxml.raxml.support --query nitrosopumilus_asvs_align_curated.fasta --model reference_uniq_raxml.raxml.bestModel
# alternatively, only show best placement
epa-ng --ref-msa all_ref_align_curated_uniq.fasta --tree reference_uniq_raxml.raxml.support --query nitrosopumilus_asvs_align_curated.fasta --model reference_uniq_raxml.raxml.bestModel --filter-max 1

# also run blast against full references (incl. redundancy)
conda activate blast-2.13.0
cat silva_genome_reference.fasta gtdb_reference_nitrosopumilus.fasta ncbi_reference_nitrosopumilus.fasta Baltic_metaG_nitrosopumilus.fasta > all_ref.fasta
makeblastdb -in all_ref.fasta -dbtype nucl -out all_ref_blastdb
blastn -query nitrosopumilus_asvs.fasta -task megablast -db all_ref_blastdb -out nitrosopumilus_asvs.blastout -evalue 1e-10 -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovs qlen gaps"