# IOWseq000013

Part of PhD thesis of Anna Wittenborn. This project investigates the link between archaeal membrane lipid composition and archaeal community composition.

### Running the pipeline for 16S Archaea

These samples were not processed as mixed orientation libraries, but with only backwards orientation.
We can use the primer-clipped (and re-reoriented) reads from LGC directly in this case.
The standard workflow had to be substantially adapted for this.

```
# copy primer-clipped to Validated_data, rename, and gzip

cd /bio/Analysis_data/IOWseq000013/Validated_data
ls -1 /bio/Raw_data/IOWseq000013/Seq_data/PrimerClipped/*/*.bz2 | while read line
do
  NEW=$(echo ${line} | xargs -n1 basename | cut -c9-)
  cp ${line} ${NEW}
done
bzip2 -d *.bz2
gzip *.fastq
```

The denoising and taxonomic classification within dada2 are documented in [this R script](https://git.io-warnemuende.de/bio_inf/IOWseq000013/src/branch/master/dada2_script.R).

Additionally, the ASVs were classified with blast against SILVA:

```
cd /bio/Analysis_data/IOWseq000013/Intermediate_results/dada2_r1
conda activate blast-2.12.0
blastn -query seqs.fasta -task blastn -db /bio/Databases/SILVA/v138.1/blast/silva_138.1_nr99_blastdb -out seqs.blastout -evalue 1e-10 -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovs qlen gaps" -max_target_seqs 100 -num_threads 64
# filter blast output
awk -v threshold=93 '($3 + $13) / 2 >= threshold' seqs.blastout > seqs.blastout_filt
# at a first look, this does not provide any further information, since the closest hits of the previously unclassified ASVs are to uncultured references with incomplete paths
```

